
// Online C++ Compiler - Build, Compile and Run your C++ programs online in your favorite browser

#include <iostream>
#include <cmath>
using namespace std;

class ConvertidorRadigra {
public:
    float radianes_a_grados(float radianes) {
        return radianes * (180.0 / 3.1416);
    }

    float grados_a_radianes(float grados) {
        return grados * (3.1416 / 180.0);
    }
};

class ConvertidorDistancias : public ConvertidorRadigra {
public:
    float km_a_millas(float kilometros) {
        if (kilometros < 0) {
            std::cerr << "Error: La cantidad de kilómetros no puede ser negativa." << endl;
            return -1;
        }
        return kilometros * 0.621371;
    }

    float millas_a_km(float millas) {
        if (millas < 0) {
           std::cerr << "Error: La cantidad de millas no puede ser negativa." << endl;
            return -1;
        }
        return millas / 0.621371;
    }
};

int main() {
    float grados, radianes, kilometros, millas, resultado;
    int opc;

    cout << "Menu" << endl;
    cout << "Selecciona lo que desea Convertir" << endl;
    cout << "1. Convertir Grados y Radianes" << endl;
    cout << "2. Convertir Kilometros y Millas" << endl;
    cin >> opc;

    switch (opc) {
        case 1:
            cout << "Selecciona la opcion deseada" << endl;
            cout << "3. Convertir Radianes a Grados" << endl;
            cout << "4. Convertir Grados a Radianes" << endl;
            cin >> opc;
            break;

        case 2:
            cout << "Selecciona la opcion deseada" << endl;
            cout << "5. Convertir Kilometros a Millas" << endl;
            cout << "6. Convertir Millas a Kilometros" << endl;
            cin >> opc;
            break;
    }

    ConvertidorRadigra convertidor;

    switch (opc) {
        case 3:
            cout << "Ingresa los grados" << endl;
            cin >> grados;
            resultado = convertidor.grados_a_radianes(grados);
            cout << "El resultado es : " << resultado << endl;
            break;

        case 4:
            cout << "Ingresa los radianes" << endl;
            cin >> radianes;
            resultado = convertidor.radianes_a_grados(radianes);
            cout << "El resultado es : " << resultado << endl;
            break;

        case 5:
            cout << "Ingresa los Kilometros" << endl;
            cin >> kilometros;
            ConvertidorDistancias convertidorDistancias;
            resultado = convertidorDistancias.km_a_millas(kilometros);
            cout << "El resultado es: " << resultado << endl;
            break;

        case 6:
            cout << "Ingresa las millas" << endl;
            cin >> millas;
            ConvertidorDistancias convertidorDistancias2;
            resultado = convertidorDistancias2.millas_a_km(millas);
            cout << "El resultado es: " << resultado << endl;
            break;
    }

    return 0;
}